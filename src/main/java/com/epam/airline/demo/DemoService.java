package com.epam.airline.demo;

import com.epam.airline.dto.Flight;
import com.epam.airline.dto.FlightCrew;
import com.epam.airline.dto.FlightCrewMember;
import com.epam.airline.dto.User;
import com.epam.airline.enums.Airliner;
import com.epam.airline.enums.Position;
import com.epam.airline.enums.Role;
import com.epam.airline.service.IFlightCrewMemberService;
import com.epam.airline.service.IFlightCrewService;
import com.epam.airline.service.IFlightService;
import com.epam.airline.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class DemoService implements IDemoService{
    @Autowired
    IFlightService flightService;

    @Autowired
    IFlightCrewService flightCrewService;

    @Autowired
    IFlightCrewMemberService flightCrewMemberService;

    @Autowired
    IUserService userService;

    @Override
    public void execute() {
        userService.createUser(new User(0, Role.ADMIN, "admin", "root", "root", "admin@admin.com"));
        System.out.println(userService.findAllUsers());

        System.out.println("---------------------------------------------------------");

        flightCrewMemberService.createFlightCrewMember(new FlightCrewMember(0, Position.PILOT, "John", "Doe",
                true, 2000));
        flightCrewMemberService.createFlightCrewMember(new FlightCrewMember(1, Position.STEWARDESS, "Jane", "Doe",
                false, 300));
        flightCrewMemberService.createFlightCrewMember(new FlightCrewMember(2, Position.NAVIGATOR, "Richard", "Stallman",
                true, 1970));
        System.out.println(flightCrewMemberService.findAllFlightCrewMembers());
        System.out.println("---------------------------------------------------------");

        flightCrewService.createFlightCrew(new FlightCrew(0, flightCrewMemberService.findAllFlightCrewMembers()));
        System.out.println(flightCrewService.findFlightCrewById(0));
        System.out.println("---------------------------------------------------------");

        flightService.createFlight(new Flight(0, Airliner.BOEING_777, "Moscow, SVO", "Izhevsk, IJK",
                LocalDateTime.of(2019, 2, 23, 18, 0,0), LocalDateTime.of(2019, 2,
                23, 20, 0,0), flightCrewService.findFlightCrewById(0)));
        System.out.println(flightService.findAllFlights());
    }
}
