package com.epam.airline.dto;

import com.epam.airline.enums.Airliner;

import java.time.LocalDateTime;

public class Flight {
    private long id;
    private Airliner airliner;
    private String fromAirport;
    private String toAirport;
    private LocalDateTime departureTime;
    private LocalDateTime arrivalTime;
    private FlightCrew flightCrew;

    public Flight(long id, Airliner airliner, String fromAirport, String toAirport, LocalDateTime departureTime,
                  LocalDateTime arrivalTime, FlightCrew flightCrew) {
        this.id = id;
        this.airliner = airliner;
        this.fromAirport = fromAirport;
        this.toAirport = toAirport;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.flightCrew = flightCrew;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Airliner getAirliner() {
        return airliner;
    }

    public void setAirliner(Airliner airliner) {
        this.airliner = airliner;
    }

    public String getFromAirport() {
        return fromAirport;
    }

    public void setFromAirport(String fromAirport) {
        this.fromAirport = fromAirport;
    }

    public String getToAirport() {
        return toAirport;
    }

    public void setToAirport(String toAirport) {
        this.toAirport = toAirport;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public FlightCrew getFlightCrew() {
        return flightCrew;
    }

    public void setFlightCrew(FlightCrew flightCrew) {
        this.flightCrew = flightCrew;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", airliner=" + airliner +
                ", fromAirport='" + fromAirport + '\'' +
                ", toAirport='" + toAirport + '\'' +
                ", departureTime=" + departureTime +
                ", arrivalTime=" + arrivalTime +
                ", flightCrew=" + flightCrew +
                '}';
    }
}
