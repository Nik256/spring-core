package com.epam.airline.dto;

import com.epam.airline.enums.Position;

public class FlightCrewMember {
    private long id;
    private Position position;
    private String name;
    private String surname;
    private boolean gender;
    private long flightHours;

    public FlightCrewMember(long id, Position position, String name, String surname, boolean gender, long flightHours) {
        this.id = id;
        this.position = position;
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.flightHours = flightHours;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public long getFlightHours() {
        return flightHours;
    }

    public void setFlightHours(long flightHours) {
        this.flightHours = flightHours;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "FlightCrewMember{" +
                "id=" + id +
                ", position=" + position +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", gender=" + gender +
                ", flightHours=" + flightHours +
                '}';
    }
}
