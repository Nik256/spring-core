package com.epam.airline.dto;

import java.util.List;

public class FlightCrew {
    private long id;
    private List<FlightCrewMember> flightCrewMembers;

    public FlightCrew(long id, List<FlightCrewMember> flightCrewMembers) {
        this.id = id;
        this.flightCrewMembers = flightCrewMembers;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<FlightCrewMember> getFlightCrewMembers() {
        return flightCrewMembers;
    }

    public void setFlightCrewMembers(List<FlightCrewMember> flightCrewMembers) {
        this.flightCrewMembers = flightCrewMembers;
    }

    @Override
    public String toString() {
        return "FlightCrew{" +
                "id=" + id +
                ", flightCrewMembers=" + flightCrewMembers +
                '}';
    }
}
