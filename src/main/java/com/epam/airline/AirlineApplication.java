package com.epam.airline;

import com.epam.airline.demo.DemoService;
import com.epam.airline.demo.IDemoService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class AirlineApplication {

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(AirlineApplication.class, args);
        IDemoService demo = applicationContext.getBean(DemoService.class);
        demo.execute();
    }

}

