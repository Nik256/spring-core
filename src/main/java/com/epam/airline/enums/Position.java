package com.epam.airline.enums;

public enum Position {
    PILOT,
    NAVIGATOR,
    OPERATOR,
    STEWARDESS
}
