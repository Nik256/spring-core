package com.epam.airline.enums;

public enum Airliner {
    BOEING_777,
    AIRBUS_A380,
    BOEING_787_DREAMLINER
}
