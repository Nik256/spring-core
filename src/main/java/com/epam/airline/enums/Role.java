package com.epam.airline.enums;

public enum Role {
    USER,
    ADMIN,
    DISPATCHER
}
