package com.epam.airline.repository;

import com.epam.airline.dto.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository implements IUserRepository {
    private List<User> users;

    public UserRepository() {
        users = new ArrayList<>();
    }

    @Override
    public void create(User user) {
        if (user != null && findByLogin(user.getLogin()) == null)
            users.add(user);
    }

    @Override
    public void update(User user) {
        if (user != null && users.contains(user))
            users.set((int) user.getId(), user);
    }

    @Override
    public void delete(User user) {
        if (user != null && users.contains(user))
            users.remove(user);
    }

    @Override
    public User findById(long id) {
        return users.get((int) id);
    }

    @Override
    public User findByLogin(String login) {
        return users.stream().filter(u -> u.getLogin().equals(login)).findFirst().orElse(null);
    }

    @Override
    public List<User> findAll() {
        return users;
    }
}
