package com.epam.airline.repository;

import com.epam.airline.dto.FlightCrew;

import java.util.List;

public interface IFlightCrewRepository {
    void create(FlightCrew flightCrew);
    void update(FlightCrew flightCrew);
    void delete(FlightCrew flightCrew);
    FlightCrew findById(long id);
    List<FlightCrew> findAll();
}
