package com.epam.airline.repository;

import com.epam.airline.dto.Flight;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface IFlightRepository {
    void create(Flight flight);
    void update(Flight flight);
    void delete(Flight flight);
    Flight findById(long id);
    List<Flight> findAll();
}
