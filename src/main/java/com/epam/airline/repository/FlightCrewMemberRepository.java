package com.epam.airline.repository;

import com.epam.airline.dto.FlightCrewMember;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class FlightCrewMemberRepository implements IFlightCrewMemberRepository {
    private List<FlightCrewMember> flightCrewMembers;

    public FlightCrewMemberRepository() {
        this.flightCrewMembers = new ArrayList<>();
    }

    @Override
    public void create(FlightCrewMember flightCrewMember) {
        if (flightCrewMember != null)
            flightCrewMembers.add(flightCrewMember);
    }

    @Override
    public void update(FlightCrewMember flightCrewMember) {
        if (flightCrewMember != null && flightCrewMembers.contains(flightCrewMember))
            flightCrewMembers.set((int) flightCrewMember.getId(), flightCrewMember);
    }

    @Override
    public void delete(FlightCrewMember flightCrewMember) {
        if (flightCrewMember != null && flightCrewMembers.contains(flightCrewMember))
            flightCrewMembers.remove(flightCrewMember);
    }

    @Override
    public FlightCrewMember findById(long id) {
        return flightCrewMembers.get((int) id);
    }

    @Override
    public List<FlightCrewMember> findAll() {
        return flightCrewMembers;
    }
}
