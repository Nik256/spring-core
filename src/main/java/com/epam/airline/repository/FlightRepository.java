package com.epam.airline.repository;

import com.epam.airline.dto.Flight;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class FlightRepository implements IFlightRepository {
    private List<Flight> flights;

    public FlightRepository() {
        this.flights = new ArrayList<>();
    }

    @Override
    public void create(Flight flight) {
        if (flight != null)
            flights.add(flight);
    }

    @Override
    public void update(Flight flight) {
        if (flight != null && flights.contains(flight))
            flights.set((int) flight.getId(), flight);
    }

    @Override
    public void delete(Flight flight) {
        if (flight != null && flights.contains(flight))
            flights.remove(flight);
    }

    @Override
    public Flight findById(long id) {
        return flights.get((int) id);
    }

    @Override
    public List<Flight> findAll() {
        return flights;
    }
}
