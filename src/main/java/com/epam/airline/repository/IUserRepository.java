package com.epam.airline.repository;

import com.epam.airline.dto.User;

import java.util.List;

public interface IUserRepository {
    void create(User user);
    void update(User user);
    void delete(User user);
    User findById(long id);
    User findByLogin(String login);
    List<User> findAll();
}
