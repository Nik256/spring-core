package com.epam.airline.repository;

import com.epam.airline.dto.FlightCrewMember;

import java.util.List;

public interface IFlightCrewMemberRepository {
    void create(FlightCrewMember flightCrewMember);
    void update(FlightCrewMember flightCrewMember);
    void delete(FlightCrewMember flightCrewMember);
    FlightCrewMember findById(long id);
    List<FlightCrewMember> findAll();
}
