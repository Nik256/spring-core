package com.epam.airline.repository;

import com.epam.airline.dto.FlightCrew;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class FlightCrewRepository implements IFlightCrewRepository {
    private List<FlightCrew> flightCrews;

    public FlightCrewRepository() {
        flightCrews = new ArrayList<>();
    }

    @Override
    public void create(FlightCrew flightCrew) {
        if (flightCrew != null)
            flightCrews.add(flightCrew);
    }

    @Override
    public void update(FlightCrew flightCrew) {
        if (flightCrew != null && flightCrews.contains(flightCrew))
            flightCrews.set((int) flightCrew.getId(), flightCrew);
    }

    @Override
    public void delete(FlightCrew flightCrew) {
        if (flightCrew != null && flightCrews.contains(flightCrew))
            flightCrews.remove(flightCrew);
    }

    @Override
    public FlightCrew findById(long id) {
        return flightCrews.get((int) id);
    }

    @Override
    public List<FlightCrew> findAll() {
        return flightCrews;
    }
}
