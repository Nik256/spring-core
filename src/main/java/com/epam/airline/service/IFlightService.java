package com.epam.airline.service;

import com.epam.airline.dto.Flight;

import java.util.List;

public interface IFlightService {
    void createFlight(Flight flight);
    void updateFlight(Flight flight);
    void deleteFlight(Flight flight);
    Flight findFlightById(long id);
    List<Flight> findAllFlights();
}
