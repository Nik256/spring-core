package com.epam.airline.service;

import com.epam.airline.dto.FlightCrewMember;

import java.util.List;

public interface IFlightCrewMemberService {
    void createFlightCrewMember(FlightCrewMember flightCrewMember);
    void updateFlightCrewMember(FlightCrewMember flightCrewMember);
    void deleteFlightCrewMember(FlightCrewMember flightCrewMember);
    FlightCrewMember findFlightCrewMemberById(long id);
    List<FlightCrewMember> findAllFlightCrewMembers();
}
