package com.epam.airline.service;

import com.epam.airline.dto.FlightCrew;
import com.epam.airline.repository.IFlightCrewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightCrewService implements IFlightCrewService {

    @Autowired
    IFlightCrewRepository flightCrewRepository;

    @Override
    public void createFlightCrew(FlightCrew flightCrew) {
        flightCrewRepository.create(flightCrew);
    }

    @Override
    public void updateFlightCrew(FlightCrew flightCrew) {
        flightCrewRepository.update(flightCrew);
    }

    @Override
    public void deleteFlightCrew(FlightCrew flightCrew) {
        flightCrewRepository.delete(flightCrew);
    }

    @Override
    public FlightCrew findFlightCrewById(long id) {
        return flightCrewRepository.findById(id);
    }

    @Override
    public List<FlightCrew> findAllFlightCrews() {
        return flightCrewRepository.findAll();
    }
}
