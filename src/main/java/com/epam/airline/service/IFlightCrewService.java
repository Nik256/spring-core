package com.epam.airline.service;

import com.epam.airline.dto.FlightCrew;

import java.util.List;

public interface IFlightCrewService {
    void createFlightCrew(FlightCrew flightCrew);
    void updateFlightCrew(FlightCrew flightCrew);
    void deleteFlightCrew(FlightCrew flightCrew);
    FlightCrew findFlightCrewById(long id);
    List<FlightCrew> findAllFlightCrews();
}
