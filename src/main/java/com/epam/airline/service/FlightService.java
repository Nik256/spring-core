package com.epam.airline.service;

import com.epam.airline.dto.Flight;
import com.epam.airline.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightService implements IFlightService {

    @Autowired
    FlightRepository flightRepository;

    @Override
    public void createFlight(Flight flight) {
        flightRepository.create(flight);
    }

    @Override
    public void updateFlight(Flight flight) {
        flightRepository.update(flight);
    }

    @Override
    public void deleteFlight(Flight flight) {
        flightRepository.delete(flight);
    }

    @Override
    public Flight findFlightById(long id) {
        return flightRepository.findById(id);
    }

    @Override
    public List<Flight> findAllFlights() {
        return flightRepository.findAll();
    }
}
