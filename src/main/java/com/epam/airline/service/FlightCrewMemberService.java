package com.epam.airline.service;

import com.epam.airline.dto.FlightCrewMember;
import com.epam.airline.repository.FlightCrewMemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightCrewMemberService implements IFlightCrewMemberService {

    @Autowired
    FlightCrewMemberRepository flightCrewMemberRepository;

    @Override
    public void createFlightCrewMember(FlightCrewMember flightCrewMember) {
        flightCrewMemberRepository.create(flightCrewMember);
    }

    @Override
    public void updateFlightCrewMember(FlightCrewMember flightCrewMember) {
        flightCrewMemberRepository.update(flightCrewMember);
    }

    @Override
    public void deleteFlightCrewMember(FlightCrewMember flightCrewMember) {
        flightCrewMemberRepository.delete(flightCrewMember);
    }

    @Override
    public FlightCrewMember findFlightCrewMemberById(long id) {
        return flightCrewMemberRepository.findById(id);
    }

    @Override
    public List<FlightCrewMember> findAllFlightCrewMembers() {
        return flightCrewMemberRepository.findAll();
    }
}
