package com.epam.airline.service;

import com.epam.airline.dto.User;

import java.util.List;

public interface IUserService {
    void createUser(User user);
    void updateUser(User user);
    void deleteUser(User user);
    User findUserById(long id);
    User findUserByLogin(String login);
    List<User> findAllUsers();
}
